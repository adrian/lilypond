Toplevel README
***************

1 Toplevel README
*****************

LilyPond is a music typesetter.  It produces beautiful sheet music
using a description file as input.  LilyPond is part of the GNU Project.

1.1 Versioning
==============

LilyPond uses a versioning scheme that easily identifies stable and
development releases.  In a version "x.y.z", an even second number 'y'
denotes a stable version.  For development versions 'y' is odd.

1.2 Downloading
===============

The primary download site for sourcecode is
`http://lilypond.org/download/'.

1.3 Compilation
===============

For compiling and running LilyPond see the installation instructions.
These instructions can be found when you unpack lilypond, as
`lilypond-x.y.z/INSTALL.txt'.  They are also available on the web at
`http://lilypond.org/doc/v2.12/Documentation/topdocs/INSTALL.html'.

1.4 Documentation
=================

The documentation is available online at `http://lilypond.org/doc/'.

   You can also build it locally: follow the instructions under
`Building documentation' in the installation instructions.

1.5 Comments
============

Send your criticism, comments, bugreports, patches, etc. to the mailing
list, not to us personally.

   We have the following mailing lists:

   * info-lilypond@gnu.org
     (http://lists.gnu.org/mailman/listinfo/info-lilypond) is a
     low-volume list for information on LilyPond project.      This
     list is moderated; ask     David R. Linn <drl@gnu.org> or
     Han-Wen <hanwen@xs4all.nl> to send announcements for this list.

   * lilypond-devel@gnu.org
     (http://lists.gnu.org/mailman/listinfo/lilypond-devel)   for
     discussions about developing LilyPond, in particular the unstable
     series.

   * lilypond-user@gnu.org
     (http://lists.gnu.org/mailman/listinfo/lilypond-user)   for
     discussions about using LilyPond, in particular the stable series.

   * bug-lilypond@gnu.org
     (http://lists.gnu.org/mailman/listinfo/bug-lilypond) for sending
     bugreports.

   * lilypond-cvs@gnu.org
     (http://lists.gnu.org/mailman/listinfo/lilypond-cvs)  for log
     files from the autobuild.

   You can search the lists from our searching facilities
(http://lilypond.org/search).

1.6 Bugs
========

Send bug reports to <bug-lilypond@gnu.org>.  For help and questions use
<lilypond-user@gnu.org>.

