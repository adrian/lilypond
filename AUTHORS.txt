AUTHORS - who did what for GNU LilyPond
***************************************

Table of Contents
*****************

AUTHORS - who did what for GNU LilyPond


This file lists authors of LilyPond, and what they wrote.  This list is
alphabetically ordered by surname.  This file lists people that have
contributed over 100 lines of code or the equivalent.

Current Development Team
------------------------

   * Bertrand Bordage: <bordage.bertrand@gmail.com>, Core developer,
     font designer

   * Trevor Daniels: <t.daniels@treda.co.uk>, Assistant documentation
     editor

   * Colin Hall: Bug meister

   * Phil Holmes: <mail@philholmes.net> `http://www.philholmes.net'
     Build unentangler, Bug squad member

   * Ian Hulin: Core developer

   * Reinhold Kainhofer: <reinhold@kainhofer.com>,
     `http://reinhold.kainhofer.com', Core developer, Music2xml wrangler

   * David Kastrup: <dak@gnu.org>, hard core developer, user and
     programming interfaces, bug squashing and swamp drainage.

   * Jonathan Kulp: Assistant documentation editor

   * Werner Lemberg, <wl@gnu.org>, Fonts, bug squasher

   * John Mandereau: <john.mandereau@free.fr>, Translations

   * Patrick McCarty: SVG guru, bug squad member, bug squasher, text
     handling

   * Joe Neeman: Core developer

   * Han-Wen Nienhuys: <hanwen@xs4all.nl>,
     `http://www.xs4all.nl/~hanwen/', Main author

   * Jan Nieuwenhuizen: <janneke@gnu.org>, `http://JoyofSource.com',
     `http://AvatarAcademy.nl', Main author

   * Graham Percival: `http://percival-music.ca', Project manager,
     Documentation Editor

   * Mark Polesky: Assistant documentation editor, Code cleanup

   * Neil Puttock: Core developer

   * Mike Solomon: <mike@mikesolomon.org>, Core developer, Frog meister

   * Carl Sorensen: <c_sorensen@byu.edu>, Core developer

   * Francisco Vila: Translation Meister

   * Valentin Villenave: LSR editor and Bug squad member

   * Janek Warchoł: happy nitpicker


Previous Development Team
-------------------------

   * Mats Bengtsson: <mats.bengtsson@ee.kth.se>,
     `https://www.kth.se/profile/matben/', Support guru

   * Pedro Kroeger: Build meister

   * John Mandereau: <john.mandereau@free.fr>, Translation meister

   * Graham Percival: `http://percival-music.ca', Bug meister, Grand
     Documentation Project leader

   * Jürgen Reuter: <reuter_j@web.de>, `http://www.juergen-reuter.de',
     Ancient notation

   * Erik Sandberg: Bug meister

   * Nicolas Sceaux: Core developer, Schemer extraordinaire


Current Contributors
--------------------

Programming
...........

Aleksandr Andreev, Frédéric Bron, Torsten Hämmerle , Marc Hohl,
James Lowe, Andrew Main, Thomas Morley, David Nalesnik, Keith OHara,
Benkő Pál, Anders Pilegaard, Julien Rioux, Johannes Rohrer, Adam
Spiers, Heikki Tauriainen

Font
....

Documentation
.............

Frédéric Bron, Federico Bruni, Colin Campbell, Urs Liska, James Lowe,
Thomas Morley, Jean-Charles Malahieude, Guy Stalnaker, Martin
Tarenskeen, Arnold Theresius, Rodolfo Zitellini

Bug squad
.........

Colin Campbell, Eluze, Marc Hohl, Phil Holmes, Marek Klein, Ralph Palmer

Support
.......

Colin Campbell, Eluze, Marc Hohl, Marek Klein, Kieren MacMillan, Urs
Liska, Ralph Palmer

Translation
...........

Federico Bruni, Luca Rossetto Casel, Felipe Castro, Pavel Fric,
Jean-Charles Malahieude, Till Paala, Yoshiki Sawada

Past Contributors
-----------------

Programming
...........

Erlend Aasland, Maximilian Albert, Aleksandr Andreev, Guido Amoruso,
Sven Axelsson, Kristof Bastiaensen, Pál Benkő, Frédéric Bron,
Juliusz Chroboczek, Peter Chubb, Angelo Contardi, Vicente Solsona Della,
Hajo Dezelski, Michael Welsh Duggan, David Feuer, Bertalan Fodor,
Richard Gay, Mathieu Giraud, Lisa Opus Goldstein, Yuval Harel, Andrew
Hawryluk, Christian Hitz, Karin Hoethker, Marc Hohl, Bernard Hurley,
Yoshinobu Ishizaki, Chris Jackson, David Jedlinsky, Heikki Junes,
Michael Käppler, Marek Klein, Michael Krause, Jean-Baptiste Lamy,
Jonatan Liljedahl, Peter Lutek, Kieren MacMillan, Hendrik Maryns,
Thomas Morgan, David Nalesnik, Matthias Neeracher, Keith OHara, Justin
Ohmie, Tatsuya Ono, Benkő Pál, Benjamin Peterson, Guy
Gascoigne-Piggford, Henning Hraban Ramm, Nathan Reed, Julien Rioux,
Stan Sanderson, Andreas Scherer, Johannes Schindelin, Patrick Schmidt,
Boris Shingarov, Kim Shrier, Edward Sanford Sutton, Adam Spiers, David
Svoboda, Heikki Taurainen, Piers Titus van der Torren, Owen Tuz,
Sebastiano Vigna, Jan-Peter Voigt, Arno Waschk, John Williams, Andrew
Wilson, Milan Zamazal, Rune Zedeler, Rodolfo Zitellini

Font
....

Tom Cato Amundsen, Marc Hohl, Chris Jackson, Alexander Kobel, Keith
OHara, Carsten Steger, Arno Waschk, Rune Zedeler

Documentation
.............

Erlend Aasland, Trevor Bača, Alard de Boer, Colin Campbell, Jay
Hamilton, Joseph Harfouch, Andrew Hawryluk, Cameron Horsburgh, Geoff
Horton, Heikki Junes, Kurtis Kroon, James Lowe, Dave Luttinen, Kieren
MacMillan, Christian Mondrup, Mike Moral, Eyolf Østrem, Ralph Palmer,
François Pinard, David Pounder, Michael Rasmussen, Till Rettig, Pavel
Roskin, Patrick Schmidt, Alberto Simoes, Anh Hai Trinh, Eduardo Vieira,
Stefan Weil, Rune Zedeler

Support
.......

Colin Campbell, Anthony Fok, Christian Hitz, Phil Holmes, Chris Jackson,
Heikki Junes, David Svoboda

Translation
...........

Alard de Boer, Federico Bruni, Abel Cheung, Frédéric Chiasson, Simon
Dahlbacka, Orm Finnendahl, David González, Nicolas Grandclaude, Dénes
Harmath, Damien Heurtebise, Bjoern Jacke, Matthieu Jacquot, Neil Jerram,
Heikki Junes, Nicolas Klutchnikoff, Jean-Charles Malahieude, Adrian
Mariano, Christian Mondrup, Tineke de Munnik, Steven Michael Murphy,
Till Paala, François Pinard, Gauvain Pocentek, Till Rettig, Ludovic
Sardain, Yoshiki Sawada, Thomas Scharkowski, Clytie Siddall, August S.
Sigov, Roland Stigge, Risto Vääräniemi, Andrea Valle, Ralf
Wildenhues, Olcay Yıldırım

